package com.company;

public class TestShapes {

    public static void main(String[] args) {
	    Rectangle rec1 = new Rectangle();
        System.out.println(rec1);
        rec1.printShape();
        System.out.println("");

        Rectangle rec2 = new Rectangle(2, 2, 2, 4);
        System.out.println(rec2);
        rec2.printShape();
        System.out.println("");

        Circle cir1 = new Circle();
        System.out.println(cir1);
        cir1.printShape();
        System.out.println("");

        Circle cir2 = new Circle(4, 5, 3);
        System.out.println(cir2);
        cir2.printShape();
        System.out.println("");
    }
}
