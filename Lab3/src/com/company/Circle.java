package com.company;

/**
 * Created by m373 on 3/13/17.
 */

public class Circle extends Shape {
    private double radius;

    Circle(){
        super(0,0);
        radius = 1;
    }

    Circle(double radius){
        super(0,0);
        this.radius = radius;
    }

    Circle(double xPos, double yPos, double radius){
        super(xPos, yPos);
        this.radius = radius;
    }

    @Override
    public void printShape() {
        System.out.println("O :)");
    }

    @Override
    public String toString() {
        return ("Circle ( " + xPos + ", " + yPos + " ), " + radius + ".");
    }

    @Override
    public double getArea() {
        return (Math.PI * radius * radius);
    }
}
