package com.company;

/**
 * Created by m373 on 3/13/17.
 */
public abstract class Shape {
    protected double xPos;
    protected double yPos;

    Shape(double xPos, double yPos){
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public void setPos(double xPos, double yPos){
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }


    @Override
    public String toString() {
        return ("Shape ( " + xPos + ", " + yPos + " ).");
    }

    abstract public double getArea();
    abstract public void printShape();
}
