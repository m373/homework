package com.company;

/**
 * Created by m373 on 3/13/17.
 */
public class Rectangle extends Shape{

    private double width;
    private double height;

    Rectangle(){
        super(0, 0);
        width = 1;
        height = 1;
    }

    Rectangle(double width, double height){
        super(0,0);
        this.width = width;
        this.height = height;
    }

    Rectangle(double xPos, double yPos, double width, double height){
        super(xPos,yPos);
        this.width = width;
        this.height = height;
    }


    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return ("Rectangle ( "+xPos + ", "+ yPos + " ) - ( " + (xPos+width) + ", " + (yPos + height) +" ).");
    }

    public void printShape(){
        for(int i = 0; i < (int)height; i++){
            for(int j = 0; j < (int)width; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
