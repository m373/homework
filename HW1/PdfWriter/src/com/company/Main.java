package com.company;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        String file = "", filename = "", path = "", content="";

        Document doc = new Document(PageSize.A5);
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the name of your document: ");
        filename = sc.nextLine()+".pdf";

        System.out.println("Enter a path to the file: ");
        path = sc.nextLine();

        System.out.println("Enter pdf's content: ");
        content = sc.nextLine();

        file = path+"/"+filename;

        PdfWriter.getInstance(doc, new FileOutputStream(file));


        doc.open();
        doc.add(new Paragraph(content));
        doc.close();
    }
}
