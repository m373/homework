package com.poocoder;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Main {

    public static boolean isPrime(int num){
        for(int i = 2; i <= sqrt(num); i++){
            if(num % i == 0)
                return false;
        }
        return  true;
    }
    public static void main(String[] args){
	    int num = 0;

        Scanner sc = new Scanner(System.in);
        try {
            num = sc.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Please enter a whole number :(");
            return;
        }

        boolean result = isPrime(num);

        if(result)
            System.out.println("The number " + num + " is prime! :D");
        else
            System.out.println("The number " + num + " is not prime! D:");
    }

}
