package com.company;

class Student{
    private String name = "";
    private String major = "";
    private String email = "";
    private int credits = 0;
    private double gpa = 0;


    public Student(){
        System.out.println("The student was initialized but must be defined :)");
    }

    public Student(String name, String major, String email, int credits, double gpa){
        this.name = name;
        this.major = major;
        this.email = email;
        this.credits = credits;
        this.gpa = gpa;
    }

    public void addToCredits(int c){
        credits += c;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }
}

public class Main {

    public static void main(String[] args) {
	    Student student = new Student("Murad Mammadov", "Programming Techniques", "muradm373@gmail.com",
                25, 0);

        student.addToCredits(10);

        System.out.println("Student "+ student.getName() + " has GPA "+ student.getGpa()+
        " on the " + student.getMajor() + " (credits: " + student.getCredits() +
                    "). You can contact him by the mail "+ student.getEmail());
    }
}
