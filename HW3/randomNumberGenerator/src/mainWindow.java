import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * Created by m373 on 3/7/2017.
 */
public class mainWindow {
    private JButton generator;
    private JLabel numHolder;
    private JPanel pane;

    public mainWindow() {
        generator.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random rn = new Random();
                int rand = Math.abs(rn.nextInt()%100+1);

                numHolder.setText(Integer.toString(rand));
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Number Generator");
        frame.setSize(500, 300);
        frame.setContentPane(new mainWindow().pane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
