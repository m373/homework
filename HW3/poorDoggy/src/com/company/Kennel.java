package com.company;


class Dog{
    private String name = "It has no name yet, could you name it? :3";
    private int age = 0;

    public Dog(){
        System.out.println("A doggy was boooorn!! *-* ");
    }
    public Dog(String name, int age){
        this.name = name;
        this.age = age;
    }
    public int humanAges(){
        return 7*age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        if(name.equals("It has no name yet, could you name it? :3"))
            return ("A doggy with no name is " + age + " years old :)");
        else
            return (name + " is " + age + " years old! :)");
    }
}


public class Kennel {

    public static void main(String[] args) {
        for(int i = 0; i < 10; i++){
            Dog doggy;
            if(i%2 == 0)
                doggy = new Dog("Doggy "+ i, i);
            else
                doggy = new Dog();
            System.out.println(doggy.toString());
        }
    }
}
