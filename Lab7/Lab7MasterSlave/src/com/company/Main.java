package com.company;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class Main {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static String DB_URL = "jdbc:mysql://localhost/";
    static String USER = "root";
    static String PASS = "";
    static ResultSet rs = null;
    static Connection conn = null;
    static Statement stmt = null;
    static int i = 1;

    public static void connect(){
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String database;
        System.out.println("Enter database name: ");
        database = sc.nextLine();
        DB_URL += database;
        System.out.println("Enter your username: ");
        USER = sc.nextLine();
        System.out.println("Enter your password: ");
        PASS = sc.nextLine();

        connect();
        
        String com = null, com2 = null, com3;

        com = "CREATE TABLE PEOPLE( ID INT PRIMARY KEY, FIRSTNAME VARCHAR(255), LASTNAME VARCHAR(255));";
        com2 = "CREATE TABLE BOOKS( ID INT PRIMARY KEY, personid int,  BOOK_NAME VARCHAR(255), " +
                " CONSTRAINT FK_PersonBook FOREIGN KEY People(personid) REFERENCES people(id));";

        try {
            stmt = conn.createStatement();
            stmt.execute(com);
            stmt.execute(com2);

            for(int i = 0; i < 5; i++){
                String add = "INSERT INTO PEOPLE VALUES("+ i + ", 'name', 'surname');";
                String add_books = "INSERT INTO books VALUES("+ i + ","+i+",'name');";

                stmt.execute(add);
                stmt.execute(add_books);
            }


            String delete = "DELETE FROM people where id = 0";

            stmt.execute(delete);



        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
