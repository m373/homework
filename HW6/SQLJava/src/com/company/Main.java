package com.company;

import java.sql.*;
import java.util.Scanner;

public class Main {

            static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
            static final String DB_URL = "jdbc:mysql://localhost/Students";

            public static void main(String[] args) {
                String USER = "root";
                String PASS = "";
                Connection conn = null;
                Statement stmt = null;
                try {
                    Scanner sc = new Scanner(System.in);

                    System.out.println("Enter your username: ");
                    USER = sc.nextLine();
                    System.out.println("Enter your password: ");
                    PASS = sc.nextLine();

                    Class.forName("com.mysql.jdbc.Driver");

                    conn = DriverManager.getConnection(DB_URL, USER, PASS);

                    System.out.println("Creating statement...");
                    stmt = conn.createStatement();
                    String sql, insert;

                    int id, age;
                    String name, surname;

                    System.out.println("Name: ");
                    name = sc.nextLine();
                    System.out.println("Surname: ");
                    surname = sc.nextLine();
                    System.out.println("Age: ");
                    age = sc.nextInt();
                    System.out.println("ID: ");
                    id = sc.nextInt();

                    insert = "INSERT INTO Students VALUES("+ id +"," + age + ", '"+ name +"', '"+surname+"');";
                    sql = "SELECT * FROM Students";
                    stmt.execute(insert);
                    ResultSet rs = stmt.executeQuery(sql);
                    System.out.println("\n\n\t\t----Table----");
                    while (rs.next()) {
                        int t_id = rs.getInt("id");
                        int t_age = rs.getInt("age");
                        String t_name = rs.getString("name");
                        String t_surname = rs.getString("surname");

                        System.out.print("ID: " + t_id);
                        System.out.print(", Age: " + t_age);
                        System.out.print(", Name: " + t_name);
                        System.out.println(", Surname: " + t_surname);
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (stmt != null)
                            stmt.close();
                    } catch (SQLException se2) {
                    }
                    try {
                        if (conn != null)
                            conn.close();
                    } catch (SQLException se) {
                        se.printStackTrace();
                    }
                }
            }
}
