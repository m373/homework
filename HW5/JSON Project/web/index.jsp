<%--
  Created by IntelliJ IDEA.
  User: m373
  Date: 3/30/2017
  Time: 2:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Person</title>
      <script src="references.js"></script>
      <script src="btnFunc.js"></script>

      <link rel="stylesheet" href="styles.css">
  </head>
  <body>

  <div class="login-page">
      <div class="form">
          <input type="text" placeholder="name" id="name"/>
          <input type="text" placeholder="surname" id="surn"/>
          <input type="email" placeholder="e-mail" id="email"/>
          <button onclick="btnClick()">Send</button>
      </div>
      <p id="JSON"></p>
  </div>

  </body>
</html>
