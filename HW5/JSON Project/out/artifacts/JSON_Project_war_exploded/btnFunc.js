/**
 * Created by m373 on 3/30/2017.
 */

function btnClick(){
    var name = $('#name').val();
    var surn = $('#surn').val();
    var email = $('#email').val();
    var data = {
        "name" : name,
        "surname" : surn,
        "email" : email
    };
    document.getElementById("JSON").innerHTML = "To see the usage of JSON have a look at console :D";

    $.ajax({
        type: "POST",
        url: "/Logger",
        data: {
            jsonData: JSON.stringify(data)
        },
        dataType: "json"
    });

    return false;
}
